Feature: add bank account to organisation 


Scenario Outline: 1. Send a message to Customer Service in the ‘Contact us’ section
	Given I am on homePage
	When I click on contactUs
	Then I send message to the <subjectHeadline> with email address <emailAddress> and order number <orderNumber>
	And I verify success message alert
		
Examples: 
   |subjectHeadline|emailAddress|orderNumber|
   |"Customer service"|"test@test.com"|123456789|


Scenario: 2. Try to create an account using already registered email id test@test.com
	Given I am on homePage
	When I click on Sign in button
	And I try to create an account using registered emailId as "test@test.com"
	Then I verify error message
		

Scenario Outline: 3. Add an item to the cart and remove the same
	Given I am on homePage
	When I add <productName> to the cart
	Then I remove <productName> from the cart
		
		Examples: 
		|productName|
		|"Printed Summer Dress"|
		
