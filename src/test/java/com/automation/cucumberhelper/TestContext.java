package com.automation.cucumberhelper;

import com.automation.driverfactory.BaseClass;
import com.automation.manager.PageObjectManager;

public class TestContext {

	private BaseClass baseClass;
	private PageObjectManager pageObjectManager;
	
	public TestContext()
	{
		baseClass = new BaseClass();
		pageObjectManager = new PageObjectManager(baseClass.getDriver());
	}
	
	public BaseClass getBaseClass()
	{
		return baseClass;
	}
	
	public PageObjectManager getPageObjectManager()
	{
		return pageObjectManager;
	}
}
