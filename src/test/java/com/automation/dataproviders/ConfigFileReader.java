package com.automation.dataproviders;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import com.automation.enums.DriverType;

public class ConfigFileReader {

	
	private Properties properties;
	private final String propertyFilepath= "src/test/resources/configs/Configuration.properties";
	
	public ConfigFileReader()
	{
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(propertyFilepath));
			properties= new Properties();
			try {
				properties.load(reader);
				reader.close();
			}catch (IOException e) {
				e.printStackTrace();
			}
		}catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException("configuration.properties not found at "+propertyFilepath);
		}
		
	}
	
	public String getChromeDriverPath()
	{
		String chromeDriverPath= properties.getProperty("chromeDriverPath");
		if(chromeDriverPath != null)
			return chromeDriverPath;
		else throw new RuntimeException("chrome driver's Path is not mentioned in the Configuraion,properties file");
		
	}
	
	public DriverType getBrowserName()
	{
		String browserName= properties.getProperty("browserName");
		if(browserName.equalsIgnoreCase("chrome")) return DriverType.CHROME;
		else if (browserName.equalsIgnoreCase("firefox")) return DriverType.FIREFOX;
		else if (browserName.equalsIgnoreCase("iexplorer")) return DriverType.INTERNETEXPLORER;
		else throw new RuntimeException("browser name is not mentioned in the Configuraion,properties file");
	}
	
	public String getApplicationUrl()
	{
		String value = properties.getProperty("url");
		if(value != null)
			return value;
		else throw new RuntimeException("url is not mentioned in the Configuraion,properties file");
	}
	
	public String getEmailId()
	{
		String value= properties.getProperty("emailId");
		if(value != null)
			return value;
		else throw new RuntimeException("EmailId is not mentioned in the Configuraion,properties file");
	}
	
	public String getMessageBody()
	{
		String value= properties.getProperty("messageBody");
		if(value != null)
			return value;
		else throw new RuntimeException("messageBody is not mentioned in the Configuraion,properties file");
	}
	
	public String getCustomerServiceFilePath()
	{
		String value = properties.getProperty("customerServicefilePath");
		if(value != null)
			return value;
		else throw new RuntimeException("customerServicefilePath is not mentioned in the Configuraion,properties file");
	}
}
