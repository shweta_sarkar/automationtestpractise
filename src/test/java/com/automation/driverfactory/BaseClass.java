package com.automation.driverfactory;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.automation.enums.DriverType;
import com.automation.manager.FileReaderManager;
import com.automation.utilities.Utility;

public class BaseClass {

	private WebDriver driver;
	private static DriverType drivertype;
	private static final String CHROME_DRIVER_PROPERTY = "webdriver.chrome.driver";

	public BaseClass() {
		drivertype = FileReaderManager.getInstance().getConfigReader().getBrowserName();
	}

	public WebDriver getDriver() {
		if (driver == null)
			driver = setDriver();

		return driver;
	}

	private WebDriver setDriver() {
		switch (drivertype) {
		case CHROME:
			getChromeDriver();
			break;

		// firefox driver code is not added
		case FIREFOX:
			getFirefoxDriver();
			break;
		// iexplorer driver code is not added
		case INTERNETEXPLORER:
			getInternetExplorer();
			break;
		}

		return driver;
	}

	public void closeDriver() {
		driver.close();
	}

	public void quitDriver() {
		driver.close();
		driver.quit();
	}

	public void getChromeDriver() {
		System.setProperty(CHROME_DRIVER_PROPERTY,
				FileReaderManager.getInstance().getConfigReader().getChromeDriverPath());
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get(FileReaderManager.getInstance().getConfigReader().getApplicationUrl());
		Utility.waitForPageLoaded(driver);

	}

	public void getFirefoxDriver() {
		// Not implemented
	}

	public void getInternetExplorer() {
		// Not implemented
	}

}
