package com.automation.enums;

public enum DriverType {
	CHROME,
	FIREFOX,
    INTERNETEXPLORER
}
