package com.automation.manager;

import org.openqa.selenium.WebDriver;

import com.automation.pageobjects.CartPage;
import com.automation.pageobjects.ContactUsPage;
import com.automation.pageobjects.HomePage;
import com.automation.pageobjects.LoginPage;

public class PageObjectManager {

	private WebDriver driver;

	private HomePage homePage;

	private ContactUsPage contactUsPage;
	
	private LoginPage loginPage;

	private CartPage cartPage;

	public PageObjectManager(WebDriver _driver)
	{
		driver = _driver;	
	}

	public HomePage getHomePage()
	{
		if(homePage==null)
			homePage= new HomePage(driver);
		return homePage;
	}

	public ContactUsPage getContactUsPage()
	{
		if(contactUsPage==null)
			contactUsPage= new ContactUsPage(driver);
		return contactUsPage;
	}

	public LoginPage getLoginPage()
	{
		if(loginPage==null)
			loginPage= new LoginPage(driver);
		return loginPage;
	}
	
	public CartPage getCartPage()
	{
		if(cartPage==null)
		cartPage= new CartPage(driver);
		return cartPage;
	}
	
}
