package com.automation.pageobjects;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.automation.utilities.Utility;

public class CartPage {

	public WebDriver driver;
	public Utility utility;

	public CartPage(WebDriver _driver) {
		driver = _driver;
		utility = new Utility(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "id_contact")
	private WebElement subjectHeadingDropDown;

	@FindAll(@FindBy(css = "[class='cart_description'] [class='product-name'] a"))
	private List<WebElement> productNameList;

	@FindAll(@FindBy(css = "[class='icon-trash']"))
	private List<WebElement> itemDeleteButton;

	@FindBy(className = "ajax_cart_quantity")
	private WebElement cartQuantity;

	public void clickOnDeleteButton(String productName) throws Throwable {
		for (int i = 0; i < productNameList.size(); i++) {
			utility.scrollWithOffset(productNameList.get(i), 10, 10);
			if (productNameList.get(i).getText().contains(productName)) {
				utility.click(itemDeleteButton.get(i), "deleteButton");
				break;
			}
		}
	}

	public boolean validatecartQuantity(int quantity) {
		return cartQuantity.getText().equals(String.valueOf(quantity));
	}

}
