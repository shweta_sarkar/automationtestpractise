package com.automation.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.automation.manager.FileReaderManager;
import com.automation.utilities.Utility;

public class ContactUsPage {
	public WebDriver driver;
	public Utility utility;

	public ContactUsPage(WebDriver _driver) {
		driver = _driver;
		utility = new Utility(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "id_contact")
	private WebElement subjectHeadingDropDown;

	@FindBy(id = "email")
	private WebElement emailIdTextBox;

	@FindBy(id = "id_order")
	private WebElement orderReferenceTextBox;

	@FindBy(id = "fileUpload")
	private WebElement chooseFileButton;

	@FindBy(id = "message")
	private WebElement messageBody;

	@FindBy(id = "submitMessage")
	private WebElement sendButton;

	@FindBy(css = "[class$='alert-success']")
	private WebElement successAlertmessage;

	public void selectSubjectHeadingFromDropDown(String subjectName) throws Throwable {
		utility.selectByVisibleText(subjectHeadingDropDown, subjectName);
	}

	public void enterEmailId(String emailAddress) throws Throwable {
		utility.sendKeys(emailIdTextBox, emailAddress);
	}

	public void enterOrderReference(int orderNumber) throws Throwable {
		utility.sendKeys(orderReferenceTextBox, String.valueOf(orderNumber));
	}

	public void enterMessageBody(String body) throws Throwable {
		utility.sendKeys(messageBody, body);
	}

	public void clickOnSendButton() throws Throwable {
		utility.click(sendButton, "send Button");
	}

	public void clickOnFileUploadbutton() throws Throwable {
		utility.Jsclick(chooseFileButton, "chooseFileButton");
	}

	public void uploadFile() throws Throwable {
		clickOnFileUploadbutton();
		utility.uploadFile(FileReaderManager.getInstance().getConfigReader().getCustomerServiceFilePath());
	}

	public boolean successAlertMessage() {
		return utility.isDisplayed(successAlertmessage);
	}
}
