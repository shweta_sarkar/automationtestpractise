package com.automation.pageobjects;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.automation.utilities.Utility;

public class HomePage {

	public WebDriver driver;
	public Utility utility;

	public HomePage(WebDriver _driver) {
		driver = _driver;
		utility = new Utility(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy(css = "a[class='login']")
	private WebElement signInButton;

	@FindBy(css = "a[title='Contact Us']")
	private WebElement contactUsButton;

	@FindBy(id = "search_query_top")
	private WebElement searchTextField;

	@FindBy(css = "button[name='submit_search']")
	private WebElement searchButton;

	@FindAll(@FindBy(css = "[class='product_img_link'] img"))
	private List<WebElement> productsList;

	@FindBy(css = "a[title='Add to cart']")
	private WebElement addToCartButton;

	@FindBy(css = "a[title='Proceed to checkout']")
	private WebElement proceedToCheckoutButton;

	public void clickOnContactUsButton() throws Throwable {
		utility.click(contactUsButton, "contactUsButton");
	}

	public void clickOnSignInbutton() throws Throwable {
		utility.click(signInButton, "sign In Button");
	}

	public void enterProductName(String productName) throws Throwable {
		utility.sendKeys(searchTextField, productName);
	}

	public void clickOnSearchbutton() throws Throwable {
		utility.click(searchButton, "searchButton");
	}

	public void clickOnAddToCartbutton() throws Throwable {
		Utility.waitForPageLoaded(driver);
		utility.scrollWithOffset(productsList.get(0), 20, 20);
		utility.MouseHover(productsList.get(0));
		utility.click(addToCartButton, "addToCartButton");
	}

	public void clickOnProceedToCartButton() throws Throwable {
		utility.click(proceedToCheckoutButton, "proceedToCheckoutButton");
	}
}
