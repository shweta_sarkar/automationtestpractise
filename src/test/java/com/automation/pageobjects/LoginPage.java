package com.automation.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.automation.utilities.Utility;

public class LoginPage {

	public WebDriver driver;
	public Utility utility;

	public LoginPage(WebDriver _driver) {
		driver = _driver;
		utility = new Utility(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "email_create")
	private WebElement createEmailId;

	@FindBy(id = "SubmitCreate")
	private WebElement createAnAccountButton;

	@FindBy(css = "[id='create_account_error'] li")
	private WebElement createAccountErrorMessage;

	public void enterEmailId(String emailAddress) throws Throwable {
		utility.sendKeys(createEmailId, emailAddress);
	}

	public void clickOnCreateAnAccountbutton() throws Throwable {
		utility.click(createAnAccountButton, "create An Account Button");
	}

	public boolean verifyCreateAccountErrorMessage() {
		return createAccountErrorMessage.isDisplayed() && createAccountErrorMessage.getText().contains(
				"An account using this email address has already been registered. Please enter a valid password or request a new one.");
	}
}
