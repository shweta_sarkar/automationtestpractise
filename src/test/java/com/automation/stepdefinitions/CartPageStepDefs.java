package com.automation.stepdefinitions;

import org.junit.Assert;

import com.automation.cucumberhelper.TestContext;
import com.automation.pageobjects.CartPage;

import io.cucumber.java.en.Then;

public class CartPageStepDefs {

	TestContext testContext;
	CartPage cartPage;

	public CartPageStepDefs(TestContext _testContext) {
		testContext = _testContext;
		cartPage = testContext.getPageObjectManager().getCartPage();
	}

	@Then("I remove {string} from the cart")
	public void i_remove_product_from_the_cart(String productName) throws Throwable {

		Assert.assertTrue("Cart quanitity invalid", cartPage.validatecartQuantity(1));
		cartPage.clickOnDeleteButton(productName);

	}

}
