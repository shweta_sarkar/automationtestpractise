package com.automation.stepdefinitions;

import org.junit.Assert;

import com.automation.cucumberhelper.TestContext;
import com.automation.pageobjects.ContactUsPage;
import com.automation.utilities.Utility;

import io.cucumber.java.en.Then;

public class ContactUsPageStepDefs {

	TestContext testContext;
	ContactUsPage contactUsPage;
	Utility utility;

	public ContactUsPageStepDefs(TestContext _testContext) {
		testContext = _testContext;
		contactUsPage = testContext.getPageObjectManager().getContactUsPage();
		utility = new Utility(testContext.getBaseClass().getDriver());
	}

	@Then("I send message to the {string} with email address {string} and order number {int}")
	public void i_send_message_to_the_with_email_address_email_and_order_number(String subjectHeadline,
			String emailAddress, Integer orderNumber) throws Throwable {
		contactUsPage.selectSubjectHeadingFromDropDown(subjectHeadline);
		contactUsPage.enterEmailId(emailAddress);
		contactUsPage.enterOrderReference(orderNumber);
		contactUsPage.uploadFile();
		contactUsPage.enterMessageBody(utility.randomString(40));
		contactUsPage.clickOnSendButton();
	}

	@Then("I verify success message alert")
	public void i_verify_success_message_alert() {
		Assert.assertTrue(contactUsPage.successAlertMessage());
	}
}
