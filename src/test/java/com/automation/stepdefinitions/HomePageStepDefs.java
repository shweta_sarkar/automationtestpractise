package com.automation.stepdefinitions;

import com.automation.cucumberhelper.TestContext;
import com.automation.pageobjects.HomePage;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;

public class HomePageStepDefs {

	TestContext testContext;
	HomePage homePage;

	public HomePageStepDefs(TestContext _testContext) {

		testContext = _testContext;
		homePage = testContext.getPageObjectManager().getHomePage();
	}

	@Given("I am on homePage")
	public void i_am_on_homePage() {

	}

	@When("I click on contactUs")
	public void i_click_on_contactUs() throws Throwable {
		homePage.clickOnContactUsButton();
	}

	@When("I click on Sign in button")
	public void i_click_on_Sign_in_button() throws Throwable {
		homePage.clickOnSignInbutton();
	}

	@When("I add {string} to the cart")
	public void i_add_one_item_to_the_cart(String productName) throws Throwable {
		homePage.enterProductName(productName);
		homePage.clickOnSearchbutton();
		homePage.clickOnAddToCartbutton();
		homePage.clickOnProceedToCartButton();
	}
}
