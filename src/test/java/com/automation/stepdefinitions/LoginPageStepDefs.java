package com.automation.stepdefinitions;

import org.junit.Assert;

import com.automation.cucumberhelper.TestContext;
import com.automation.pageobjects.LoginPage;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;

public class LoginPageStepDefs {

	TestContext testContext;
	LoginPage loginPage;

	public LoginPageStepDefs(TestContext _testContext) {

		testContext = _testContext;
		loginPage = testContext.getPageObjectManager().getLoginPage();
	}

	@And("I try to create an account using registered emailId as {string}")
	public void i_try_to_create_an_account_using_registered_emailId(String email) throws Throwable {
		loginPage.enterEmailId(email);
		loginPage.clickOnCreateAnAccountbutton();
	}

	@Then("I verify error message")
	public void i_verify_error_message() {
		Assert.assertTrue("Error message not displayed", loginPage.verifyCreateAccountErrorMessage());
	}
}
